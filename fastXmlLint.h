int set_schema(const char *xsdData);

//sets a schema, returns true (1) on success and false (0) if an error occurred
//needs a filePATH
int set_schema_file(const char *xsdPath);

//validates an XML-File given as String
//precondition: a schema is set with set_schema()
const char* validate(const char *xmlData);

//sets an relaxNG schema, returns true (1) on success and false (0) if an error occurred
int set_relaxNG_schema(const char *ngData);

//sets an relaxNG schema from an given File, returns true (1) on success and false (0) if an error occurred
//this is needed when the schema is split into multiple files
int set_relaxNG_schema_file(const char *ngPath);

//validates an XML-File given as String
//precondition: a schema is set with set_schema()
const char* validate_NG(const char *xmlData);

void cleanup();
