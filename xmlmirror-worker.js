/*global console, postMessage, importScripts, self, setSchema, setRelaxNGSchema, validate, validateNG*/
var initialized = false,
    relax_ng = false;
/**
* This function is called when emscripten finishes loading fastXmlLint.js
*/
function EM_XMLonRuntimeInitialized() {
    "use strict";
    initialized = true;
    postMessage(["init"]);
}

// import the xml linter
importScripts("fastXmlLint.js");

/**
* Handle incoming validation requests.
*/
self.addEventListener("message", function (m) {
    "use strict";
    if (!initialized) {
        postMessage(["error", "Validator has not initialized yet."]);
        return;
    }
    var d = m.data[1],
        result;
    try {
        if (m.data[0] === "s") {
            relax_ng = d.indexOf("http://relaxng.org/ns/structure/1.0") !== -1;
            if (relax_ng) {
                setRelaxNGSchema(d);
            } else {
                setSchema(d);
            }
            postMessage(["schema", null]);
        } else if (relax_ng) {
            result = validateNG(d);
            postMessage([null, result]);
        } else {
            result = validate(d);
            postMessage([null, result]);
        }
    } catch (e) {
        console.log("exception: ", e);
        postMessage(["error", "R.I.P.\nemscripten probably ran out of memory, you can only reload the page now and contact an developer to increase memory size or find other fixes.", e]);
    }
});
