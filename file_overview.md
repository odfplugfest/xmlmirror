# File overview for XmlMirror

## `customserver.py`

Python script that can launch a web server that serves files from the current folder via http on port 8000.

## `default.nix`

Environment description for two environments: `nativeEnv` and `emEnv`. These environments can be entered with the commands
```bash
nix-shell -A nativeEnv
```
and
```bash
nix-shell -A emEnv
```

The environment `nativeEnv` is used for building the native binary `fastXmlLint` from `fastXmlLint.c`. The commands to run this build are given in `Makefile.nativeEnv`.

`nix-shell -A nativeEnv` sets the alias
```bash
alias make="colormake -f Makefile.emEnv"
```
so in `nativeEnv`, `make` builds `fastXmlLint`.

The environment `emEnv` is used for building 

## `DemoAutocomplete.js` `DemoAutoComplete.xhtml`

A demo that shows how schemaInfo objects are created from a Relax NG schema and used in CodeMirror.

## `DemoMultifile.js` `DemoMultifile.xhtml`

A demo that shows how a schema with multiple files can be loaded.

## `DemoRelaxNGAsync.js` `DemoRelaxNGAsync.xhtml` `DemoRelaxNGWebWorker.js`

A demo that shows that XML can be validated asynchroneaiously with a web worker.

## `DemoRelaxNG.xhtml` `DemoRelaxNG.js`

A demonstration of validation a document with respect to a Relax NG schema.

## `DemoUltimate.js` `DemoUltimate.xhtml`

fail_example1.xml

fail_example2.xml

fail.html

`fastXmlLint.c`

`file_overview.md`

This file.

html5.xsd
index.xhtml
jquery-2.2.3.min.js
Makefile.emEnv
Makefile.nativeEnv
OpenDocument-schema-v1.1-errata01-complete.rng
OpenDocument-schema-v1.1-errata01-complete.schemaInfo.json
pre.js
README.md
schemainfoCreator-helpers.js
schemainfoCreator.js
schemainfoCreator-test.html
selenium_test.py
success_example1.xml
success_example2.xml
success_example3.xml
success.html
test2.js
test_fail1.html
test.js
test_success1.html
xml-lint-RelaxNG.js
xml.xsd




TODO:

  add 'make clean'
  
