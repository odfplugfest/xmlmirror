/*global window, CodeMirror, document, XMLHttpRequest, toSchemaInfo, console, alert, loadRng, XMLSerializer*/

// initializes the code mirror editor for XML
function createXMLEditor(id, schemaInfo) {
    "use strict";
    return CodeMirror.fromTextArea(document.getElementById(id), {
        mode: "xml",
        lineNumbers: true,
        gutters: ["CodeMirror-lint-markers"],
        hintOptions: {schemaInfo: schemaInfo},
        extraKeys: {
            "'<'": "completeAfter",
            "'/'": "completeIfAfterLt",
            "' '": "completeIfInTag",
            "'='": "completeIfInTag",
            "Ctrl-Space": "autocomplete"
        }
    });
}

// initializes the code mirror editor for the Relax NG schema
function createCodeView(id, mode) {
    "use strict";
    return CodeMirror.fromTextArea(document.getElementById(id), {
        mode: mode,
        lineNumbers: true,
        gutters: ["CodeMirror-lint-markers"],
        readOnly: true
    });
}

window.addEventListener("load", function () {
    "use strict";
    loadRng("libreoffice-relax-ng-example/OpenDocument-strict-schema-v1.1-errata01-complete.rng", function (err, xml) {
        if (err) {
            alert(err);
            return;
        }
        if (!xml) {
            alert("Cannot load XML.");
        }
        // convert the XMLDocument into an schemaInfo object
        var schemaInfo = toSchemaInfo(xml),
            json = JSON.stringify(schemaInfo, null, 2),
            txt = (new XMLSerializer()).serializeToString(xml);

        // create the code mirror editors
        createCodeView("xsdeditor", "xml").setValue(txt);
        createCodeView("schemaInfo", "javascript").setValue(json);
        createXMLEditor("xmleditor", schemaInfo);
    });
});
