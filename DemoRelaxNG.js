/*global $, document, setRelaxNGSchema, console, CodeMirror, clearTimeout,
  setTimeout, setSchema, alert*/

function timerExpired(xsdeditor) {
    "use strict";
    var message = xsdeditor.getValue();
    if (setSchema(message) === 0) {
        console.log("setSchema call fail");
    } else {
        console.log("setSchema call success");
    }
}

// http://ejohn.org/blog/how-javascript-timers-work/
function resetTimerOnChange(timer, xsdeditor) {
    "use strict";
    clearTimeout(timer);
    return setTimeout(function () {
        timerExpired(xsdeditor);
    }, 2000);
}

//creates the CodeMirror editor
function createXMLEditor(id, validate) {
    "use strict";
    var editor = CodeMirror.fromTextArea(document.getElementById(id), {
        mode: "xml",
        lineNumbers: true,
        gutters: ["CodeMirror-lint-markers"],
        lint: validate
    });
    console.log("xml created");
    return editor;
}

// If this function is deffined it is used by 'fastXmlLint.js' and called as
// soon as it has finished loading
function EM_XMLonRuntimeInitialized() {
    "use strict";
    $(document).ready(function () {
        $.ajax({
            url: "OpenDocument-schema-v1.1-errata01-complete.rng",
            type: "GET",
            dataType: "text",
            success: function (data) {
                setRelaxNGSchema(data);

                var xsdeditor = createXMLEditor("xsdeditor", false),
                    timer = 0;
                xsdeditor.setValue(data);
                xsdeditor.on("change", function () {
                    timer = resetTimerOnChange(timer, xsdeditor);
                });
                console.log("xsd schema set");

                createXMLEditor("xmleditor", true);
            },
            error: function () {
                alert("Couldn't load Schema File");
            }
        });
    });
}
