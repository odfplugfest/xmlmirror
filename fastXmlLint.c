#include <string.h>

#include <libxml/parser.h>
#include <libxml/valid.h>
#include <libxml/xmlschemas.h>
#include <libxml/relaxng.h>

#include "json.h"

//xml Schema is saved in here
xmlSchemaValidCtxtPtr validCtxt = NULL;
xmlSchemaPtr xmlschema = NULL;
xmlRelaxNGValidCtxtPtr validctxtNG = NULL;
xmlRelaxNGPtr rngSchema = NULL;

//errors found in an xml file
json_object *jarray = NULL;

#define UNUSED(x) (void)(x)

void cleanup() {
  if (validCtxt != NULL) {
    xmlSchemaFreeValidCtxt(validCtxt);
    validCtxt = NULL;
  }
  if (xmlschema != NULL) {
    xmlSchemaFree(xmlschema);
    xmlschema = NULL;
  }
  if (rngSchema != NULL) {
    xmlRelaxNGFree(rngSchema);
    rngSchema = NULL;
  }
  if (validctxtNG != NULL) {
    xmlRelaxNGFreeValidCtxt(validctxtNG);
    validctxtNG = NULL;
  }
  if (jarray != NULL){
    json_object_put(jarray); //free memory
    jarray = NULL;
  }
}

//removes new lines from a string
char* strip(char *s) {
    char *r = s;
    char *p2 = s;
    while(*s != '\0') {
    	if(*s != '\n') {
    		*p2++ = *s++;
    	} else {
    		++s;
    	}
    }
    *p2 = '\0';
    return r;
}

// callback function which handles parse/schema errors and fills a json object
void handleStructedError(void* userData, xmlErrorPtr error) {
    UNUSED(userData);

    json_object *jsonReport = json_object_new_object();

    json_object_object_add(jsonReport, "domain",  json_object_new_int(error->domain));
    json_object_object_add(jsonReport, "code",    json_object_new_int(error->code));
    json_object_object_add(jsonReport, "message", json_object_new_string(strip(error->message)));
    json_object_object_add(jsonReport, "level", json_object_new_int(error->level));

    json_object_object_add(jsonReport, "line",    json_object_new_int(error->line));

    if (error->str1 != NULL){
      json_object_object_add(jsonReport, "str1", json_object_new_string(strip(error->str1)));
    }else{
      json_object_object_add(jsonReport, "str1", json_object_new_string(""));
    }
    if (error->str2 != NULL){
      json_object_object_add(jsonReport, "str2", json_object_new_string(strip(error->str2)));
    }else{
      json_object_object_add(jsonReport, "str2", json_object_new_string(""));
    }
    if (error->str3 != NULL){
      json_object_object_add(jsonReport, "str3", json_object_new_string(strip(error->str3)));
    }else{
      json_object_object_add(jsonReport, "str3", json_object_new_string(""));
    }

    json_object_object_add(jsonReport, "int1",    json_object_new_int(error->int1));
    json_object_object_add(jsonReport, "int2",    json_object_new_int(error->int2));

    json_object_array_add(jarray,jsonReport);
}

//sets a schema, returns true (1) on success and false (0) if an error occurred
int set_schema(const char *xsdData) {
    cleanup();
    xmlSchemaParserCtxtPtr parserCtxt = xmlSchemaNewMemParserCtxt(xsdData, strlen(xsdData));
    if (parserCtxt == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }
    xmlschema = xmlSchemaParse(parserCtxt);
    xmlSchemaFreeParserCtxt(parserCtxt);
    if (xmlschema == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }

    validCtxt = xmlSchemaNewValidCtxt(xmlschema);
    if (validCtxt == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }
    return 1;
}

//sets a schema, returns true (1) on success and false (0) if an error occurred
//needs a filePATH
int set_schema_file(const char *xsdPath) {
    cleanup();
    xmlSchemaParserCtxtPtr parserCtxt = xmlSchemaNewParserCtxt(xsdPath);
    if (parserCtxt == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }
    xmlschema = xmlSchemaParse(parserCtxt);
    xmlSchemaFreeParserCtxt(parserCtxt);
    if (xmlschema == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }

    validCtxt = xmlSchemaNewValidCtxt(xmlschema);
    if (validCtxt == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }
    return 1;
}

//validates an XML-File given as String
//precondition: a schema is set with set_schema()
const char* validate(const char *xmlData) {
    // schema validation errors will now have meaningfull line numbers and not constantly 0
    xmlLineNumbersDefault(1);
    if (jarray != NULL){
      json_object_put(jarray); //free memory
    }
    jarray = json_object_new_array();

    //check if schema is set.
    if (validCtxt == NULL) {
        fprintf(stderr, "schema has to be set with 'set_schema' first\n");
        return "schema has to be set with 'set_schema'";
    }

    //set callback function
    xmlSetStructuredErrorFunc(NULL, handleStructedError);

    //oparse xmlDATA
    int xmlLength = strlen(xmlData);
    xmlDocPtr xml = xmlParseMemory(xmlData, xmlLength);

    //validate xml DATA
    xmlSchemaValidateDoc(validCtxt, xml);
    xmlFreeDoc(xml);

    //convert json object to strings
    return json_object_to_json_string(jarray);
}

//sets an relaxNG schema, returns true (1) on success and false (0) if an error occurred
int set_relaxNG_schema(const char *ngData){
    cleanup();

    xmlRelaxNGParserCtxtPtr parserCtxt = xmlRelaxNGNewMemParserCtxt(ngData, strlen(ngData));
    if (parserCtxt == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }

    rngSchema = xmlRelaxNGParse(parserCtxt);
    xmlRelaxNGFreeParserCtxt(parserCtxt);
    if (rngSchema == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }

    validctxtNG = xmlRelaxNGNewValidCtxt(rngSchema);

    return 1;
}

//sets an relaxNG schema from an given File, returns true (1) on success and false (0) if an error occurred
//this is needed when the schema is split into multiple files
int set_relaxNG_schema_file(const char *ngPath){
    cleanup();

    xmlRelaxNGParserCtxtPtr parserCtxt = xmlRelaxNGNewParserCtxt(ngPath);
    if (parserCtxt == NULL){
      fprintf(stderr, "error while parsing schema");
      return 0;
    }

    rngSchema = xmlRelaxNGParse(parserCtxt);
    xmlRelaxNGFreeParserCtxt(parserCtxt);
    if (rngSchema == NULL){
      fprintf(stderr, "error while parsing schema");
      return -1;
    }

    validctxtNG = xmlRelaxNGNewValidCtxt(rngSchema);

    return 1;
}


//validates an XML-File given as String
//precondition: a schema is set with set_schema()
const char* validate_NG(const char *xmlData) {
    // schema validation errors will now have meaningfull line numbers and not constantly 0
    xmlLineNumbersDefault(1);
    if (jarray != NULL){
      json_object_put(jarray); //free memory
    }
    jarray = json_object_new_array();

    //check if schema is set.
    if (validctxtNG == NULL) {
        fprintf(stderr, "schema has to be set with 'set_relaxNG_schema' first\n");
        return "schema has to be set with 'set_relaxNG_schema'";
    }

    //set callback function
    xmlSetStructuredErrorFunc(NULL, handleStructedError);

    //oparse xmlDATA
    int xmlLength = strlen(xmlData);
    xmlDocPtr xml = xmlParseMemory(xmlData, xmlLength);

    //validate xml DATA
    xmlRelaxNGValidateDoc(validctxtNG, xml);
    xmlFreeDoc(xml);
    //convert json object to strings
    return json_object_to_json_string(jarray);
}
