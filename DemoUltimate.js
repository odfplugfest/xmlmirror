/*global Worker, document, alert, console, $, jQuery, toSchemaInfo, CodeMirror, loadRng, XMLSerializer*/

var xmleditor,
    worker = new Worker("xmlmirror-worker.js"),
    update,
    time,
    // only send a message if the worker is currently idle
    workerAvailable = true,
    // store exactly one job if the worker is currently unavaible
    jobToSend = "",
    state = document.getElementById("state"),
    spinner = document.getElementById("spinner"),
    schemaInfo = "",
    firstErrorPosition = 0;

/**
* Does the async validation
*/
function validateXML(text, updateLinting) {
    "use strict";

    state.innerHTML = "validating...";
    state.style.color = "black";
    spinner.setAttribute("class", "spinner");

    update = updateLinting;
    if (workerAvailable) {
        workerAvailable = false;
        console.log("start validating");
        time = Date.now();
        worker.postMessage(["v", text]);
    } else {
        console.log("worker not avaiable");
        jobToSend = text;
    }
}

/**
* creates the CodeMirror editor
*/
function createXMLEditor(id) {
    "use strict";
    var editor = CodeMirror.fromTextArea(document.getElementById(id), {
        mode: "xml",
        lineNumbers: true,
        gutters: ["CodeMirror-lint-markers"],
        lint: {
            "getAnnotations": validateXML,
            "async": true
        },
        hintOptions: {schemaInfo: schemaInfo},
        extraKeys: {
            "'<'": "completeAfter",
            "'/'": "completeIfAfterLt",
            "' '": "completeIfInTag",
            "'='": "completeIfInTag",
            "Ctrl-Space": "autocomplete"
        }
    });
    console.log("xml created with validation");

    editor.setOption("lint.xml.async", true);
    return editor;
}
/**
* loads the schema file and initialices the editor
*/
function initEditor() {
    "use strict";
    console.log("init editor");
    $(document).ready(function () {
        loadRng("libreoffice-relax-ng-example/OpenDocument-strict-schema-v1.1-errata01-complete.rng", function (err, xml) {
            if (err || !xml) {
                alert("Couldn't load Schema File");
                return;
            }
            var data = (new XMLSerializer()).serializeToString(xml);

            //convert the XMLDocument into an schemaInfo object
            schemaInfo = toSchemaInfo(xml);
            //start the worker
            worker.postMessage(["s", data]);
            console.log("xsd schema set");
            xmleditor = createXMLEditor("xmleditor", true);
        });
    });
}

//be carefull this implementation only works with a single codeMirror editor.
worker.onmessage = function (e) {
    "use strict";
    workerAvailable = true;
    if (e.data[0] === "init") {
        initEditor();
    } else if (e.data[0] === "error") {
        alert(e.data[1]);
    } else {
        var jo,
            line,
            success = false,
            lines = e.data[0].split("\n"),
            found = [],
            i = 0;
        try {
            //'validateNG(txt)' gave back a string containing JSON
            jo = JSON.parse(e.data[1]);

            success = true;
        } catch (err) {
            console.log("could not parse json:", err);
        }
        if (success) {
            //the root element of 'jo' is an array. Every element is an error.
            //this while loop translates every XML error into somthing CodeMirror
            //understands
            while (i < jo.length) {
                line = Math.max(jo[i].line - 1, 0);
                //sets the jump position for the input button
                if (i === 0) {
                    firstErrorPosition = line;
                }
                found.push({
                    from: new CodeMirror.Pos(line, 0),
                    to: new CodeMirror.Pos(line, lines[line].length),
                    severity: "error",
                    message: jo[i].message
                });
                i += 1;
            }
        }
        //update state
        spinner.setAttribute("class", "");
        if (found.length === 0) {
            state.innerHTML = "successfully validated";
            state.style.color = "green";
            $("#firstErrorButton").prop("disabled", true);
        } else {
            state.innerHTML = "validated with errors";
            state.style.color = "red";
            $("#firstErrorButton").prop("disabled", false);
        }

        //update CodeMirror
        update(xmleditor, found);

        //this is not actually necessary but it's a nice demonstration of the speed
        //that can be accomplished when using 'fastXmlLint.js'
        console.log("finished validating");
        console.log("time used (in ms): " + (Date.now() - time));
    }
    if (jobToSend !== "") {
        validateXML(jobToSend, update);
        jobToSend = "";
    }
};

/*jslint: unused*/
function jumpFirstErrorPosition() {
    "use strict";
    var t = xmleditor.charCoords({
            line: firstErrorPosition,
            ch: 0
        }, "local").top,
        middleHeight = xmleditor.getScrollerElement().offsetHeight / 2;
    xmleditor.scrollTo(null, t - middleHeight - 7);
}
