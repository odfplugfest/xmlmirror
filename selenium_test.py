from selenium import webdriver
from selenium import selenium
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0

import sys
import os

#from pyvirtualdisplay import Display
#display = Display(visible=0, size=(800, 600))
#display.start()

from distutils.version import LooseVersion, StrictVersion
if LooseVersion(webdriver.__version__) < LooseVersion("2.51"):
    sys.exit("error: version of selenium ("
        + str(LooseVersion(webdriver.__version__))
        + ") is too old, needs 2.51 at least")

ff = webdriver.Firefox()

#ff.get("file:///home/joachim/Desktop/projects/nlnet/nlnet/schemainfoCreator-test.html")
st = "file:///" + os.getcwd() + "/schemainfoCreator-test.html"
ff.get(st)

assert "schemaInfo unit-test" in ff.title
try:
    element = WebDriverWait(ff, 10).until(EC.presence_of_element_located((By.ID, "OK")))
finally:
    ff.quit()
