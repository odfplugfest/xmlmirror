/*global rngns, isRng*/
/** @typedef {{
 children: !Array.<!string>,
 attrs:    !Object<!string,?Array<!string>>
}} */
var ElementDef;

/**
 * Collect all <define/> elements.
 * @param {!Document} doc
 * @return {!Object<!string,!Array.<!Element>>}
 */
function collectDefinitions(doc) {
    "use strict";
    var /**@type{!Object<!string,!Array.<!Element>>}*/
        definitions = {},
        defs = doc.getElementsByTagNameNS(rngns, "define"),
        i,
        name,
        def,
        array;
    for (i = 0; i < defs.length; i += 1) {
        def = defs.item(i);
        name = def.getAttribute("name");
        array = definitions[name] = definitions[name] || [];
        array.push(def);
    }
    return definitions;
}

/**
 * Continue recursion in the definition elements for the given reference.
 * @param {!Object<!string,!Array.<!Element>>} defs
 * @param {!Array.<!string>} stack
 * @param {!Element} ref
 * @param {!function(!Element)} handler
 */
function followReference(defs, stack, ref, handler) {
    "use strict";
    var name = ref.getAttribute("name").trim();
    if (stack.indexOf(name) === -1) { // avoid infinite loop
        stack.push(name);
        defs[name].map(handler);
        stack.pop();
    }
}

/**
 * Recurse into the child elements. Follow references.
 * @param {!Object<!string,!Array.<!Element>>} defs
 * @param {!Array.<!string>} stack
 * @param {!Element} rng
 * @param {!function(!Element)} handler
 */
function recurseRng(defs, stack, rng, handler) {
    "use strict";
    var child;
    if (isRng(rng, "ref")) {
        followReference(defs, stack, rng, function (e) {
            handler(e);
        });
    } else {
        child = rng.firstElementChild;
        while (child) {
            handler(child);
            child = child.nextElementSibling;
        }
    }
}

/**
 * Collect the text from all the <value/> elements.
 * @param {!Object<!string,!Array.<!Element>>} defs
 * @param {!Array.<!string>} stack
 * @param {!Element} rng
 * @param {!Array<!string>} values
 */
function getAttributeValues(defs, stack, rng, values) {
    "use strict";
    var text;
    if (isRng(rng, "value")) {
        text = rng.textContent.trim();
        if (values.indexOf(text) === -1) {
            values.push(text);
        }
    } else {
        recurseRng(defs, stack, rng, function (e) {
            getAttributeValues(defs, stack, e, values);
        });
    }
}

/**
 * Get the possible names for an element or attribute.
 * @param {!Element} e
 * @param {!Array.<!string>} names
 */
function getNamesRecurse(e, names) {
    "use strict";
    var child;
    if (isRng(e, "name")) {
        names.push(e.textContent);
    } else if (isRng(e, "choice")) {
        child = e.firstElementChild;
        while (child) {
            getNamesRecurse(child, names);
            child = child.nextElementSibling;
        }
    }
}

/**
 * Get the possible names for an element or attribute.
 * @param {!Element} e
 * @return {!Array.<!string>}
 */
function getNames(e) {
    "use strict";
    if (e.hasAttribute("name")) {
        return [e.getAttribute("name")];
    }
    var names = [],
        child = e.firstElementChild;
    while (child) {
        getNamesRecurse(child, names);
        child = child.nextElementSibling;
    }
    return names;
}

/**
 * Find the allowed child elements and attributes for an element.
 * @param {!Object<!string,!Array.<!Element>>} defs
 * @param {!Array.<!string>} stack
 * @param {!Element} rng
 * @param {!ElementDef} def
 */
function defineElement(defs, stack, rng, def) {
    "use strict";
    var names,
        values = [];
    if (isRng(rng, "element")) {
        names = getNames(rng);
        names.map(function (name) {
            if (def.children.indexOf(name) === -1) {
                def.children.push(name);
            }
        });
    } else if (isRng(rng, "attribute")) {
        getAttributeValues(defs, stack, rng, values);
        names = getNames(rng);
        if (values.length === 0) {
            values = null;
        }
        names.map(function (name) {
            if (def.attrs[name]) {
                def.attrs[name] = def.attrs[name].concat(values);
            } else {
                def.attrs[name] = values;
            }
        });
    } else if (isRng(rng, "text")) {
        def.text = true;
    } else {
        recurseRng(defs, stack, rng, function (e) {
            defineElement(defs, stack, e, def);
        });
    }
}

/**
 * @param {!Object.<!string,!Object>} unordered
 * @return {!Object}
 */
function sortObject(unordered) {
    "use strict";
    var ordered = {},
        keys = Object.keys(unordered);
    keys.sort();
    keys.map(function copy(key) {
        ordered[key] = unordered[key];
    });
    return ordered;
}

/**
 * @param {!Object.<!string,?Array.<!string>>} attrs
 */
function sortAttributeValues(attrs) {
    "use strict";
    var keys = Object.keys(attrs);
    keys.map(function (key) {
        var a = attrs[key];
        if (a) {
            a.sort();
        }
    });
}

/**
 * Find all elements and return their definitions.
 * @param {!Object<!string,!Array.<!Element>>} defs
 * @param {!Element} rng
 * @param {!Object<!string,!ElementDef>} elements
 */
function findElements(defs, rng, elements) {
    "use strict";
    var child,
        names,
        element;
    if (isRng(rng, "element")) {
        element = {attrs: {}, children: []};
        child = rng.firstElementChild;
        while (child) {
            defineElement(defs, [], child, element);
            child = child.nextElementSibling;
        }
        element.children.sort();
        element.attrs = sortObject(element.attrs);
        sortAttributeValues(element.attrs);
        names = getNames(rng);
        names.map(function (name) {
            elements[name] = element;
        });
    } else {
        child = rng.firstElementChild;
        while (child) {
            findElements(defs, child, elements);
            child = child.nextElementSibling;
        }
    }
}

/**
 * @param {!Object<!string,!Array.<!Element>>} defs
 * @param {!Array.<!string>} stack
 * @param {!Element} rng
 * @param {!Array.<!string>} top
 */
function findTopLevelElements(defs, stack, rng, top) {
    "use strict";
    if (rng.localName === "element") {
        if (rng.hasAttribute("name")) {
            top.push(rng.getAttribute("name"));
        }
    } else {
        recurseRng(defs, stack, rng, function (e) {
            findTopLevelElements(defs, stack, e, top);
        });
    }
}

/**
 * @param {!Object<!string,!Array.<!Element>>} defs
 * @param {!Array.<!string>} stack
 * @param {!Document} doc
 * @return {!Array.<!string>}
 */
function findAllTopLevelElements(defs, stack, doc) {
    "use strict";
    var top = [],
        starts = doc.getElementsByTagNameNS(rngns, "start"),
        e,
        i;
    for (i = 0; i < starts.length; i += 1) {
        e = /**@type{!Element}*/(starts.item(i));
        findTopLevelElements(defs, stack, e, top);
    }
    return top;
}

/**
 * Converts a RelaxNG schema file to an SchemaInfo object which can be read by
 * codemirror and used for autocompletion.
 *
 * @param {!Document} doc
 * @return {!Object}
 */
function toSchemaInfo(doc) {
    "use strict";
    var definitions = collectDefinitions(doc),
        elements = {};
    Object.keys(definitions).map(function (key) {
        definitions[key].map(function (define) {
            findElements(definitions, define, elements);
        });
    });
    elements["!top"] = findAllTopLevelElements(definitions, [], doc);
    elements = sortObject(elements);
    return elements;
}
