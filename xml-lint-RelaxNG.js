/*global module, exports, require, define, CodeMirror, console, validateNG*/

//Defines the linter function for codemirror
(function (mod) {
    "use strict";
    if (typeof exports === "object" && typeof module === "object") { // CommonJS
        mod(require("../../lib/codemirror"));
    } else if (typeof define === "function" && define.amd) { // AMD
        define(["../../lib/codemirror"], mod);
    } else { // Plain browser env
        mod(CodeMirror);
    }
}(function (CodeMirror) {
    "use strict";
    CodeMirror.registerHelper("lint", "xml", function (text) {
        console.log("start validating");
        var found = [],
            time = (new Date()).getTime(),
            // the actually validation of the document. Make sure that
            // 'setRelaxNGSchema' is called successfully before you are calling
            // this function
            r = validateNG(text),
            jo,
            line,
            success = false,
            lines = text.split("\n"),
            i = 0;
        try {
            //'validateNG(txt)' gave back a string containing JSON
            jo = JSON.parse(r);
            success = true;
        } catch (e) {
            console.log('could not parse json:', r);
        }
        if (success) {
            // the root element of 'jo' is an array. Every element is an error.
            // this while loop translates every XML error into somthing
            // CodeMirror understands
            while (i < jo.length) {
                line = Math.max(jo[i].line - 1, 0);
                found.push({
                    from: CodeMirror.Pos(line, 0),
                    to: CodeMirror.Pos(line, lines[line].length),
                    severity: "error",
                    message: jo[i].message
                });
                i += 1;
            }
        }
        // this is not actually necessary but it's a nice demonstration of the
        // speed that can be accomplished when using 'fastXmlLint.js'
        console.log(r);
        console.log("finished validating");
        console.log("time used (in ms): " + ((new Date()).getTime() - time));
        return found;
    });
}));
