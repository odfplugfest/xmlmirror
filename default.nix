with import <nixpkgs> { };

let

############## emscripten environment below #################################################

libz = stdenv.mkDerivation {
  name="libz";

  buildInputs = [ emscripten autoconf automake libtool pkgconfig gnumake ];

   src = fetchurl {
     url = "http://zlib.net/zlib-1.2.8.tar.gz";
     sha256 = "039agw5rqvqny92cpkrfn243x2gd4xn13hs3xi6isk55d2vqqr9n";
   };

  configurePhase = ''
    export EMCONFIGURE_JS=2 

    # Some tests require writing at $HOME
    HOME=$TMPDIR

    emconfigure ./configure --prefix=$out
  '';

  buildPhase = ''
    emmake make
  '';
};

json-c = stdenv.mkDerivation rec {
  name = "json-c";

  buildInputs = [ emscripten autoconf automake libtool pkgconfig gnumake ];

  version = "json-c-0.12-20140410";

  src = fetchgit {
    url = "https://github.com/json-c/json-c";
    rev = "refs/tags/${version}";
    sha256 = "1cj9qvsc8djgs0x9g3xb7szgjpmyqkw4gr3pf26qal35lv2wb1ig";
  };

  configurePhase = ''
    export EMCONFIGURE_JS=2

    # Some tests require writing at $HOME
    HOME=$TMPDIR

    ./autogen.sh
    emconfigure ./configure --prefix=$out
  '';

  buildPhase = ''
    emmake make
  '';

};

xml-js = stdenv.mkDerivation rec {
  name = "xml-js";

  buildInputs = [ emscripten autoconf automake libtool pkgconfig gnumake ];

  src = fetchgit {
    url = "https://github.com/kripken/xml.js";
    rev = "95addcf845fa8393f3d3b1a0f747404a1a51ec2c";
    sha256 = "1sj8y3aakvlhgrwwyhpyq2kn7h53gclck4w0mwvakh8k5fjlgff9";
  };

  configurePhase = ''
    cd libxml2
    export EMCONFIGURE_JS=2

    # Some tests require writing at $HOME
    HOME=$TMPDIR
    autoreconf -if -Wall

    emconfigure ../libxml2/configure --with-http=no --with-ftp=no --with-python=no --with-threads=no --prefix=$out CFLAGS='-O3' CXXFLAGS='-O3'
#--with-debug
  '';

  buildPhase = ''
    emmake make CFLAGS='-O3' CXXFLAGS='-O3'
  '';
};

# tested to work with emscripten 1.35.4
fastXmlLint = stdenv.mkDerivation rec {
  name = "fastXmlLint";

  buildInputs = [ json-c libz xml-js emscripten gnumake pkgconfig ];

  phases= [ "unpackPhase" "patchPhase" "configurePhase" "buildPhase" "checkPhase" "installPhase" ] ;

  src = ./.;

  configurePhase = ''
    export EMCONFIGURE_JS=2

    # Some tests require writing at $HOME
    HOME=$TMPDIR
  '';

  # wrapping make using 'emmake' would remove the required exports for pkg-config to function properly
  buildPhase = ''
    make
  '';

  checkPhase = "";

  # this doesn't make much sense yet
  installPhase = ''
    mkdir $out/
    cp -R *.js* $out/
    cp -R *.xhtml $out/
    cp -R *.html $out/
    cp -R codemirror-5.12 $out/
    cp -R html5-rng $out/
    cp -R libreoffice-relax-ng-example $out/
  '';
};

emEnvironment = stdenv.mkDerivation rec {

  name = "emEnv";

  shellHook = ''
    export HISTFILE=".zsh_history"
    alias make="colormake -f Makefile.emEnv"
    alias c="while true; do inotifywait * -e modify --quiet > /dev/null; clear; make closure| head -n 30; done"
    alias s="python customserver.py"
    alias jcc=closure_compiler/jcc
    echo "welcome to the emEnvironment"
    PS1="emEnv: \$? \w \[$(tput sgr0)\]"
  '';

  buildInputs = [ json-c libz xml-js ] ++ [ colormake nodejs emscripten autoconf automake libtool pkgconfig gnumake strace ltrace python openjdk ncurses ];
};

############## emscripten environment above #################################################



############## native environment below #################################################

json-c-native = stdenv.mkDerivation rec {
  name = "json-c-native";

  buildInputs = [ autoconf automake libtool pkgconfig gnumake ];

  version = "json-c-0.12-20140410";

  src = fetchgit {
    url = "https://github.com/json-c/json-c";
    rev = "refs/tags/${version}";
    sha256 = "1cj9qvsc8djgs0x9g3xb7szgjpmyqkw4gr3pf26qal35lv2wb1ig";
  };

  configurePhase = ''
    ./autogen.sh
    sed 's/-Werror//g' -i Makefile.in linkhash.c Makefile.am.inc configure aclocal.m4
    ./configure --prefix=$out
  '';

  buildPhase = ''
    make
  '';
};

nativeEnvironment = stdenv.mkDerivation rec {
  buildInputs = [ json-c-native libxml2 nodejs colormake autoconf automake libtool pkgconfig gnumake strace ltrace python openjdk ncurses ];
  name = "nativeEnv";

  shellHook = ''
    export HISTFILE=".zsh_history"
    alias make="colormake -f Makefile.nativeEnv"
    alias c="while true; do inotifywait * -e modify --quiet > /dev/null; clear; make closure| head -n 30; done"
    alias s="python customserver.py"
    alias jcc=closure_compiler/jcc
    echo "welcome to the nativeEnvironment"
    PS1="nativeEnv: \$? \w \[$(tput sgr0)\]"
  '';


};

############## native environment above #################################################

in

{
  # use nix-shell with -A to select the wanted environment to work with:
  #   --pure is optional

  # nix-shell -A nativeEnv --pure  
  nativeEnv = nativeEnvironment;
  # nix-shell -A emEnv --pure  
  emEnv = emEnvironment;
}

