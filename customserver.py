import SimpleHTTPServer
import SocketServer
PORT = 8000

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
Handler.extensions_map.update({
        '.html':  'application/xhtml+xml',
        '.xhtml': 'application/xhtml+xml',
        '.js':    'application/javascript',
        '.rng':   'application/xml',
        '.xsd':   'application/xml',
        '.css':   'text/css',
        '.md':    'text/markdown',
        '.jpg':   'image/jpeg',
        '.png':   'image/png',
        '.svg':   'image/svg+xml'
});

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "Serving at port", PORT
httpd.serve_forever()

