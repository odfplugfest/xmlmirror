/*global CodeMirror, loadRng, toSchemaInfo, console, XMLSerializer, Worker, DOMParser*/

/**
@typedef{{
  from: !CodeMirror.Pos,
  to: !CodeMirror.Pos,
  severity: !string,
  message: !string
}}*/
var CmError;

/**@typedef{!Array.<!CmError>}*/
var CmErrors;

/**
@typedef{{
  setSchema: !function(!string,!function(?string):undefined=),
  validate: !function(!string,!function(!CmErrors))
}}*/
var Validator;
/**
 * @param {?string=} scriptUrlPrefix
 * @return {!Validator}
 */
function createValidator(scriptUrlPrefix) {
    "use strict";
    var /**@type{?Worker}*/
        worker,
        /**@type{?string}*/
        schemaXml = null,
        /**@type{!boolean}*/
        initialized = false,
        /**@type{?function(!CmErrors):undefined}*/
        callback = null,
        /**@type{?function(!CmErrors):undefined}*/
        newCallback = null,
        /**@type{?function(?string)}*/
        schemaCallback = null,
        /**@type{?string}*/
        newText = null,
        scriptPath = "xmlmirror-worker.js";
    if (scriptUrlPrefix) {
        scriptPath = scriptUrlPrefix + scriptPath;
    }
    /**
     * @param {!Array.<{line:!number,message:!string}>} xmlErrors
     * @return {!CmErrors}
     */
    function parseXmlErrors(xmlErrors) {
        var i = 0,
            line,
            cmErrors = [],
            /**@type{!CodeMirror.Pos}*/
            from,
            /**@type{!CodeMirror.Pos}*/
            to;
        // translate libxml2 error to CodeMirror error
        while (i < xmlErrors.length) {
            line = Math.max(xmlErrors[i].line - 1, 0);
            from = new CodeMirror.Pos(line, 0);
            to = new CodeMirror.Pos(line, 100);
            cmErrors.push({
                from: from,
                to: to,
                severity: "error",
                message: xmlErrors[i].message
            });
            i += 1;
        }
        return cmErrors;
    }
    /**
     * @param {MessageEvent.<*>} e
     */
    function onmessage(e) {
        var data = /**@type{!Array.<!string>}*/(e.data);
        if (data[0] === "schema" && schemaCallback) {
            schemaCallback(data[1]);
            return;
        }
        if (data[0] === "error") {
            console.log(data[1]);
            console.log(data[2]);
            worker.terminate();
            worker = new Worker(scriptPath);
            worker.onmessage = onmessage;
            initialized = false;
            if (schemaCallback) {
                schemaCallback(data[1] + "\n" + data[2]);
            }
            return;
        }
        if (schemaXml && (initialized || data[0] === "init")) {
            worker.postMessage(["s", schemaXml]);
            schemaXml = null;
        }
        if (data[0] === "init") {
            initialized = true;
            return;
        }
        if (!callback) {
            return;
        }
        // result of validation
        var result = /**@type{!Array.<{line:!number,message:!string}>}*/(JSON.parse(data[1])),
            errors = parseXmlErrors(result);
        callback(errors);
        if (newCallback) {
            callback = newCallback;
            worker.postMessage(["v", newText]);
            newCallback = null;
            newText = null;
        } else {
            callback = null;
        }
    }
    /**
     * @param {!string} schema
     * @param {!function(?string):undefined} callback
     */
    function setSchema(schema, callback) {
        schemaCallback = callback;
        if (initialized) {
            worker.postMessage(["s", schema]);
        } else {
            schemaXml = schema;
        }
    }
    /**
     * @param {!string} text
     * @param {!function(!CmErrors):undefined} cb
     */
    function validate(text, cb) {
        if (schemaXml && initialized) {
            worker.postMessage(["s", schemaXml]);
            schemaXml = null;
        }
        if (callback || !initialized) {
            // already a validation in progress, put this one on hold
            newCallback = cb;
            newText = text;
            return;
        }
        callback = cb;
        worker.postMessage(["v", text]);
    }
    try {
        worker = new Worker(scriptPath);
    } catch (/**@type{!string}*/e) {
        console.log(e);
    }
    if (worker) {
        worker.onmessage = onmessage;
    }
    return {
        setSchema: setSchema,
        validate: validate
    };
}

/**
 * Look for <parsererror/>.
 * Unfortunately, when there is no parsing error, this still scans the
 * entire document.
 *
 * @param {!Element} e
 * @return {!boolean}
 */
function isParserError(e) {
    "use strict";
    if (e.localName === "parsererror") {
        return true;
    }
    var c = e.firstElementChild;
    while (c) {
        if (isParserError(c)) {
            return true;
        }
        c = c.nextElementSibling;
    }
    return false;
}

/**
 * @param {!Element} e
 */
function sortAttributes(e) {
    "use strict";
    var atts = {},
        a,
        i,
        keys;
    for (i = 0; i < e.attributes.length; i += 1) {
        a = /**@type{!Attr}*/(e.attributes.item(i));
        atts[a.name] = a;
    }
    keys = Object.keys(atts);
    keys.map(function (key) {
        e.removeAttributeNode(atts[key]);
    });
    keys.sort();
    keys.map(function (key) {
        e.setAttributeNode(atts[key]);
    });
}

/**
 * @param {!Element} e
 * @param {!Object.<!string,!{text:!boolean}>} schema
 * @param {!string} indent
 */
function formatElement(e, schema, indent) {
    "use strict";
    var c,
        ce,
        n,
        subindent = indent + "  ";
    sortAttributes(e);
    if (schema[e.tagName] && !schema[e.tagName].text) {
        c = e.firstChild;
        while (c) {
            n = c.nextSibling;
            // remove whitespace nodes
            if (c.nodeType === 3 && !(/\S/.test(c.nodeValue))) {
                e.removeChild(c);
            }
            c = n;
        }
        c = e.firstChild;
        while (c) {
            if (c.nodeType === 1 || c.nodeType === 8) {
                e.insertBefore(e.ownerDocument.createTextNode(subindent), c);
            }
            c = c.nextSibling;
        }
        if (e.firstElementChild) {
            e.appendChild(e.ownerDocument.createTextNode(indent));
        }
    }
    ce = e.firstElementChild;
    while (ce) {
        formatElement(ce, schema, subindent);
        ce = ce.nextElementSibling;
    }
}

/**
 * Format the XML
 * If the XML is not well-formed this function returns null.
 * @param {!string} value
 * @param {!Object.<!string,!{text:!boolean}>} schema
 * @return {?string}
 */
function formatXml(value, schema) {
    "use strict";
    var parser = new DOMParser(),
        dom = parser.parseFromString(value, "text/xml"),
        newValue;
    if (isParserError(dom.documentElement)) {
        return null;
    }
    formatElement(dom.documentElement, schema, "\n");
    newValue = (new XMLSerializer()).serializeToString(dom);
    var firstTag = "<" + dom.documentElement.tagName,
        firstTagPos = newValue.indexOf(firstTag);
    if (firstTagPos > 1) {
        // add newline after xml declaration before first element
        newValue = newValue.replace(firstTag, "\n" + firstTag);
        if (newValue.indexOf("?><!") < firstTagPos) {
            // add newline between xml declaration and DOCTYPE
            newValue = newValue.replace("?><!", "?>\n<!");
        }
    }
    return newValue;
}

/**
 * @param {!string} uri
 * @param {!Object} schemaInfo
 * @param {!Validator} validator
 * @param {!function(?string):undefined} callback
 */
function setSchema(uri, schemaInfo, validator, callback) {
    "use strict";
    // clear old schemaInfo
    Object.keys(schemaInfo).map(function (key) {
        delete schemaInfo[key];
    });
    loadRng(uri, function (err, doc) {
        if (err || !doc) {
            return callback(err);
        }
        var newSchemaInfo = toSchemaInfo(doc),
            schemaXml = (new XMLSerializer()).serializeToString(doc);
        Object.keys(newSchemaInfo).map(function (key) {
            schemaInfo[key] = newSchemaInfo[key];
        });
        if (validator) {
            validator.setSchema(schemaXml, callback);
        } else {
            callback(null);
        }
    });
}

/**
 * @constructor
 */
function XmlEditorOptions() {
    "use strict";
    /**@type{?function(!string):!string}*/
    this.beforeValidate = null;
    /**@type{?function(!string):!string}*/
    this.beforeFormat = null;
    /**@type{?function(!string):!string}*/
    this.afterFormat = null;
    /**@type{?function(!Object)}*/
    this.postProcessSchemaInfo;
    /**@type{?string}*/
    this.scriptUrlPrefix = null;
}

/**
 * @param {!HTMLTextAreaElement} textarea
 * @param {!XmlEditorOptions=} options
 * @param {!string=} schemaUri
 */
function createXmlEditor(textarea, options, schemaUri) {
    "use strict";
    var schemaInfo = {};
    options = options || new XmlEditorOptions();

    /**
     * @param {!CodeMirror} cm
     * @param {?function():boolean} pred
     * @return {!number}
     */
    function completeAfter(cm, pred) {
        if (!pred || pred()) {
            setTimeout(function () {
                if (!cm.state.completionActive) {
                    cm.showHint({completeSingle: false});
                }
            }, 100);
        }
        return CodeMirror.Pass;
    }

    /**
     * @param {!CodeMirror} cm
     * @return {!number}
     */
    function completeIfAfterLt(cm) {
        return completeAfter(cm, function () {
            var cur = cm.getCursor(),
                pos = new CodeMirror.Pos(cur.line, cur.ch - 1);
            return cm.getRange(pos, cur) === "<";
        });
    }

    /**
     * @param {!CodeMirror} cm
     * @return {!number}
     */
    function completeIfInTag(cm) {
        return completeAfter(cm, function () {
            var tok = cm.getTokenAt(cm.getCursor());
            if (tok.type === "string" &&
                    (!(/['"]/).test(tok.string.charAt(tok.string.length - 1))
                    || tok.string.length === 1)) {
                return false;
            }
            var inner = CodeMirror.innerMode(cm.getMode(), tok.state).state;
            return inner.tagName;
        });
    }

    /**@type{!Validator}*/
    var validator;
    var annotations = null;
    /**
     * @param {!string} text
     * @param {!function(!CmErrors):undefined} callback
     */
    function validate(text, callback) {
        if (options.beforeValidate) {
            text = options.beforeValidate(text);
        }
        if (validator) {
            validator.validate(text, function (errors) {
                callback(errors);
                if (annotations) {
                    annotations.update(errors);
                }
            });
        } else {
            callback([]);
        }
    }
    /**
     * @param {!CodeMirror} cm
     */
    function format(cm) {
        var value = cm.getValue(),
            cursor = cm.getCursor(),
            newValue;
        if (options.beforeFormat) {
            value = options.beforeFormat(value);
        }
        newValue = formatXml(value, schemaInfo);
        if (newValue === null) {
            return;
        }
        if (options.afterFormat) {
            newValue = options.afterFormat(newValue);
        }
        if (value !== newValue) {
            cm.setValue(newValue);
            cm.setCursor(cursor);
        }
    }
    var defaultOptions = {
        mode: "xml",
        lineNumbers: true,
        lineWrapping: true,
        gutters: ["CodeMirror-lint-markers", "CodeMirror-foldgutter"],
        lint: {
            "getAnnotations": validate,
            "async": true
        },
        hintOptions: {schemaInfo: schemaInfo, closeCharacters: /\ \/>/},
        matchTags: {bothTags: true},
        autoCloseTags: true,
        foldGutter: true,
        highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true},
        extraKeys: {
            "'<'": completeAfter,
            "'/'": completeIfAfterLt,
            "' '": completeIfInTag,
            "'='": completeIfInTag,
            "Ctrl-Space": "autocomplete",
            "Ctrl-J": "toMatchingTag",
            "Shift-Ctrl-F": format
        }
    };
    Object.keys(defaultOptions).map(function (key) {
        if (!options[key]) {
            options[key] = defaultOptions[key];
        }
    });
    var editor = CodeMirror.fromTextArea(textarea, options);
    if (editor.annotateScrollbar) {
        annotations = editor.annotateScrollbar("xmlmirror-error");
    }
    validator = createValidator(options.scriptUrlPrefix);
    /**
     * @param {!string} uri
     */
    editor.setSchema = function (uri) {
        setSchema(uri, schemaInfo, validator, function (err) {
            if (err) {
                console.log(err);
                return;
            }
            if (options.postProcessSchemaInfo) {
                options.postProcessSchemaInfo(schemaInfo);
            }
            if (editor.performLint) {
                editor.performLint();
            }
        });
    };
    if (schemaUri) {
        editor.setSchema(schemaUri);
    }
    return editor;
}
