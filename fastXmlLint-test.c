#include "fastXmlLint.h"
#include <libxml/parser.h>

#include <sys/stat.h>

//get the size of an text file
u_int32_t get_file_size(const char *file_name) {
    struct stat buf;
    if ( stat(file_name, &buf) != 0 ) return(0);
    return (unsigned int)buf.st_size;
}
//reads a text file
char* readFile(const char* xmlPath) {
    int xmlLength = get_file_size(xmlPath);
    char *xmlSource = malloc(sizeof(char) * xmlLength + 1);

    FILE *p = fopen(xmlPath, "r");
    char c;
    unsigned int i = 0;
    while ((c = fgetc(p)) != EOF) {
        xmlSource[i++] = c;
    }
    fclose(p);
    xmlSource[xmlLength] = 0;
    return xmlSource;
}
//some simple tests
int main () {

  const char* schema = "html5.xsd";
  const char* multiFileSchemaNG = "xhtml-rng/xhtml.rng";
  const char* multiFileSchemaNGOpenDocument = "libreoffice-relax-ng-example/OpenDocument-strict-schema-v1.1-errata01-complete.rng";
  const char* test1 = "success.html";
  const char* test2 = "fail.html";
  const char* testOD1 = "success_example1.xml";
  const char* testOD2 = "success_example2.xml";
  const char* testOD3 = "fail_example1.xml";
  const char* testOD4 = "fail_example2.xml";
  char* contents;


  fprintf(stderr, "============= reading schema: %s =============\n", schema);
  contents = readFile(schema);
  int r = set_schema(contents);
  free(contents);
  if (r == 0) {
      fprintf(stderr, "Could not create XSD schema validation context.\n");
      exit(1);
  }
  fprintf(stderr, "============= validate test1 (expected result: success): %s =============\n", test1);
  contents = readFile(test1);
  const char* result1 = validate(contents);
  free(contents);
  fprintf(stderr, "Validation:\n%s\n", result1);

  fprintf(stderr, "============= validate test2 (expected result: fail): %s =============\n", test2);
  contents = readFile(test2);
  const char* result2 = validate(contents);
  free(contents);
  fprintf(stderr, "Validation:\n%s\n", result2);

  fprintf(stderr, "============= reading RELAX NG multifile schema: %s =============\n", multiFileSchemaNG);
  set_relaxNG_schema_file(multiFileSchemaNG);
  fprintf(stderr, "============= validate test1 (expected result: success): %s =============\n", test1);
  contents = readFile(test1);
  const char* result3 = validate_NG(contents);
  free(contents);
  fprintf(stderr, "Validation NG1:\n%s\n", result3);
  fprintf(stderr, "============= validate test2 (expected result: fail): %s =============\n", test2);
  contents = readFile(test2);
  const char* result4 = validate_NG(contents);
  free(contents);
  fprintf(stderr, "Validation NG2:\n%s\n", result4);

  fprintf(stderr, "============= reading RELAX NG multifile schema: %s =============\n", multiFileSchemaNGOpenDocument);
  set_relaxNG_schema_file(multiFileSchemaNGOpenDocument);

  fprintf(stderr, "============= validate test1 (expected result: success): %s =============\n", testOD1);
  contents = readFile(testOD1);
  fprintf(stderr, "Validation OD:\n%s\n",validate_NG(contents));
  free(contents);
  fprintf(stderr, "============= validate test2 (expected result: success): %s =============\n", testOD2);
  contents = readFile(testOD2);
  fprintf(stderr, "Validation OD:\n%s\n",validate_NG(contents));
  free(contents);
  fprintf(stderr, "============= validate test3 (expected result: fail): %s =============\n", testOD3);
  contents = readFile(testOD3);
  fprintf(stderr, "Validation OD:\n%s\n",validate_NG(contents));
  free(contents);
  fprintf(stderr, "============= validate test4 (expected result: fail): %s =============\n", testOD4);
  contents = readFile(testOD4);
  fprintf(stderr, "Validation OD:\n%s\n",validate_NG(contents));
  free(contents);
  /*
*/
  cleanup();
  xmlCleanupParser();
  return 0;
}
