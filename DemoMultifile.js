/*global document, $, CodeMirror, console, FS, setRelaxNGSchemaFile, alert*/
var xmleditor;

// creates the CodeMirror editor
function createXMLEditor(id, validate) {
    "use strict";
    var editor = CodeMirror.fromTextArea(document.getElementById(id), {
        mode: "xml",
        lineNumbers: true,
        gutters: ["CodeMirror-lint-markers"],
        lint: validate
    });
    console.log("xml created");
    return editor;
}

// loads all needed files and writes them into the virtual file sistem
function loadSchema() {
    "use strict";
    var files = [
            "exclude/basic-table.rng",
            "exclude/basic.rng",
            "exclude/form.rng",
            "modules/applet.rng",
            "modules/attribs.rng",
            "modules/base.rng",
            "modules/basic-form.rng",
            "modules/basic-table.rng",
            "modules/bdo.rng",
            "modules/csismap.rng",
            "modules/datatypes.rng",
            "modules/edit.rng",
            "modules/events.rng",
            "modules/form.rng",
            "modules/frames.rng",
            "modules/hypertext.rng",
            "modules/iframe.rng",
            "modules/image.rng",
            "modules/inlstyle.rng",
            "modules/legacy.rng",
            "modules/link.rng",
            "modules/list.rng",
            "modules/meta.rng",
            "modules/nameident.rng",
            "modules/object.rng",
            "modules/param.rng",
            "modules/pres.rng",
            "modules/ruby.rng",
            "modules/script.rng",
            "modules/ssismap.rng",
            "modules/struct.rng",
            "modules/style.rng",
            "modules/table.rng",
            "modules/target.rng",
            "modules/text.rng",
            "xhtml-basic.rng",
            "xhtml-strict.rng",
            "xhtml.rng"
        ],
        folders = ["modules", "exclude"],
        i,
        finishedCounter = 0;
    // create all folders
    folders.map(function (folder) {
        FS.createFolder("/", folder, true, true);
    });

    // load and create all files
    function callback() {
        // checks if all files are loaded
        finishedCounter += 1;
        if (finishedCounter === files.length) {
            console.log("all files loaded");
            // all files have finished loading
            console.log(setRelaxNGSchemaFile("/xhtml.rng"));
            // initiate the codemirror editor
            xmleditor = createXMLEditor("xmleditor", true);
        }
    }

    function loadFile(filename) {
        $.ajax({
            url: "xhtml-rng/" + filename,
            type: "GET",
            dataType: "text",
            success: function (data) {
                var split = filename.split("/");
                FS.createDataFile("/" + split[0], split[1], data, true, true);
                console.log("loaded " + filename);
                callback();
            },
            error: function () {
                alert("Couldn't load Schema File");
            }
        });
    }
    files.map(function (file) {
        loadFile(file);
    });
}

// If this function is defined it is used by 'fastXmlLint.js' and called as
// soon as it has finished loading
function EM_XMLonRuntimeInitialized() {
    "use strict";
    $(document).ready(function () {
        loadSchema();
    });
}
