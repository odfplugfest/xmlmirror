// This file contains code to load Relax NG files.
// The code roughly follows the specification at
// http://relaxng.org/spec-20011203.html#IDAG3YR

var rngns = "http://relaxng.org/ns/structure/1.0";

/**
 * AJAX call to load an xml file
 * @param {!string} uri
 * @param {!function(?string,?Document)} callback
 */
function loadXML(uri, callback) {
    "use strict";
    var xhr = new XMLHttpRequest();
    xhr.overrideMimeType("text/xml");
    xhr.open("GET", uri, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status >= 200 && xhr.status < 300 && xhr.responseXML) {
                callback(null, xhr.responseXML);
            } else {
                callback("Cannot load " + uri + ".", null);
            }
        }
    };
    xhr.send(null);
}

/**
 * @param {!string} uri
 * @param {!function(?string,?Document):undefined} callback
 */
var loadRng;

/**
 * Only handles relative paths!
 * @param {!string} base
 * @param {!string} path
 * @return {!string}
 */
function resolveUrl(base, path) {
    "use strict";
    var pos = base.lastIndexOf("/");
    if (pos !== -1) {
        base = base.substr(0, pos);
    }
    return base + "/" + path;
}

/**
 * Check if an element is an RNG element with the given local-name.
 * @param {!Element} e
 * @param {!string} name
 * @return {!boolean}
 */
function isRng(e, name) {
    "use strict";
    return e.namespaceURI === rngns && e.localName === name;
}

/**
 * @param {!Element} e
 * @return {?Element}
 */
function getStart(e) {
    "use strict";
    var c = e.firstElementChild;
    while (c) {
        if (isRng(c, "start")) {
            return c;
        }
        c = c.nextElementSibling;
    }
    return null;
}

/**
 * @param {!Element} e
 */
function removeStartElements(e) {
    "use strict";
    var c = e.firstElementChild,
        n;
    while (c) {
        n = c.nextElementSibling;
        if (isRng(c, "start")) {
            e.removeChild(c);
        }
        c = n;
    }
}

/**
 * @param {!Element} e
 * @param {!Object.<!string,!boolean>} defines
 */
function removeOverriddenDefines(e, defines) {
    "use strict";
    var c = e.firstElementChild,
        n;
    while (c) {
        n = c.nextElementSibling;
        if (isRng(c, "define") && defines[c.getAttribute("name").trim()]) {
            e.removeChild(c);
        }
        c = n;
    }
}

/**
 * @param {!Element} e
 * @return {!Object.<!string,!boolean>}
 */
function getDefineNames(e) {
    "use strict";
    var c = e.firstElementChild,
        names = {};
    while (c) {
        if (isRng(c, "define")) {
            names[c.getAttribute("name").trim()] = true;
        }
        c = c.nextElementSibling;
    }
    return names;
}

/**
 * @param {!Element} a
 * @param {!Element} b
 */
function copyMissingGrammarAttributes(a, b) {
    "use strict";
    var i,
        att;
    for (i = 0; i < a.attributes.length; i += 1) {
        att = /**@type{!Attr}*/(a.attributes.item(i));
        if (!b.hasAttributeNS(att.namespaceURI, att.name)) {
            b.setAttributeNS(att.namespaceURI, att.name, att.value);
        }
    }
}

/**
 * @param {!string} uri
 * @param {!Document} doc
 * @param {!Element} include
 * @param {!function(?string):undefined} callback
 */
function resolveInclude(uri, doc, include, callback) {
    "use strict";
    var path = include.getAttribute("href"),
        includeUri = resolveUrl(uri, path);
    loadRng(includeUri, function (err, includeDoc) {
        if (err) {
            return callback(err);
        }
        // include roughly according to section '4.7. include element'
        var grammar = /**@type{!Element}*/(include.parentNode),
            includeGrammar = includeDoc.documentElement,
            start = getStart(include),
            defines = getDefineNames(include);
        includeGrammar = /**@type{!Element}*/(doc.importNode(includeGrammar, true));
        if (start) {
            removeStartElements(includeGrammar);
        }
        removeOverriddenDefines(includeGrammar, defines);
        while (includeGrammar.firstElementChild) {
            grammar.insertBefore(includeGrammar.firstElementChild, include);
        }
        while (include.firstElementChild) {
            grammar.insertBefore(include.firstElementChild, include);
        }
        copyMissingGrammarAttributes(includeGrammar, grammar);
        grammar.removeChild(include);
        callback(null);
    });
}

/**
 * @param {!string} uri
 * @param {!Document} doc
 * @param {!function(?string):undefined} callback
 */
function resolveIncludes(uri, doc, callback) {
    "use strict";
    function loop() {
        var includes = doc.getElementsByTagNameNS(rngns, "include");
        if (includes.length === 0) {
            return callback(null);
        }
        var include = /**@type{!Element}*/(includes.item(0));
        resolveInclude(uri, doc, include, function (err) {
            if (err) {
                return callback(err);
            }
            loop();
        });
    }
    loop();
}

/**
 * @param {!string} uri
 * @param {!function(?string,?Document):undefined} callback
 */
loadRng = function (uri, callback) {
    "use strict";
    loadXML(uri, function (err, doc) {
        if (err || !doc) {
            return callback(err, doc);
        }
        resolveIncludes(uri, doc, function (err) {
            if (err) {
                return callback(err, null);
            }
            callback(null, doc);
        });
    });
};
