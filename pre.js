var setSchema,
    setSchemaFile,
    validate,
    setRelaxNGSchema,
    setRelaxNGSchemaFile,
    validateNG;

var Module = {
  'onRuntimeInitialized':  function() {
    "use strict";

    //export all functions
    // window['setSchema'] = Module.cwrap('set_schema', 'bool', ['string']);
    // window['setSchemaFile'] = Module.cwrap('set_schema_file', 'bool', ['string']);
    // window['validate'] = Module.cwrap('validate', 'string', ['string']);
    // window['setRelaxNGSchema'] = Module.cwrap('set_relaxNG_schema', 'bool', ['string']);
    // window['setRelaxNGSchemaFile'] = Module.cwrap('set_relaxNG_schema_file', 'bool', ['string']);
    // window['validateNG'] = Module.cwrap('validate_NG', 'string', ['string']);

    setSchema = Module.cwrap('set_schema', 'bool', ['string']);
    setSchemaFile = Module.cwrap('set_schema_file', 'bool', ['string']);
    validate = Module.cwrap('validate', 'string', ['string']);
    setRelaxNGSchema = Module.cwrap('set_relaxNG_schema', 'bool', ['string']);
    setRelaxNGSchemaFile = Module.cwrap('set_relaxNG_schema_file', 'bool', ['string']);
    validateNG = Module.cwrap('validate_NG', 'string', ['string']);

    //run callback
    if (typeof EM_XMLonRuntimeInitialized == 'function')
      EM_XMLonRuntimeInitialized();

  }
};
