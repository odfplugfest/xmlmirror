/**
 * @constructor
 */
function CodeMirror() {
    "use strict";
}
/**
 * @constructor
 * @param {!number} line
 * @param {!number} col
 */
CodeMirror.Pos = function (line, col) {
    "use strict";
};
/**
 * @constructor
 */
CodeMirror.Cursor = function () {
    "use strict";
};
/**@type{!number}*/
CodeMirror.Cursor.prototype.line;
/**@type{!number}*/
CodeMirror.Cursor.prototype.ch;
/**
 * @constructor
 */
CodeMirror.Mode = function () {
    "use strict";
};
/**
 * @constructor
 */
CodeMirror.State = function () {
    "use strict";
};
/**@type{!boolean}*/
CodeMirror.State.prototype.tagName;
/**
 * @type{{completionActive:!boolean}}
 */
CodeMirror.prototype.state;
/**
 * @param {!{completeSingle:!boolean}} options
 */
CodeMirror.prototype.showHint = function (options) {
    "use strict";
};
/**
 * @return {!CodeMirror.Cursor}
 */
CodeMirror.prototype.getCursor = function () {
    "use strict";
};
/**
 * @param {!CodeMirror.Cursor} cursor
 */
CodeMirror.prototype.setCursor = function (cursor) {
    "use strict";
};
/**
 * @return {!CodeMirror.Mode}
 */
CodeMirror.prototype.getMode = function () {
    "use strict";
};
/**
 * @param {!CodeMirror.Pos} pos
 * @param {!CodeMirror.Cursor} cursor
 * @return {!string}
 */
CodeMirror.prototype.getRange = function (pos, cursor) {
    "use strict";
};
/**
 * @param {!CodeMirror.Cursor} cur
 * @return {!{type:!string,string:!string,state:!CodeMirror.State}}
 */
CodeMirror.prototype.getTokenAt = function (cur) {
    "use strict";
};
/**
 * @param {!string} className
 * @return {!{update: function(!CmErrors):undefined}}
 */
CodeMirror.prototype.annotateScrollbar = function (className) {
    "use strict";
};
CodeMirror.prototype.performLint = function () {
    "use strict";
};
/**
 * @return {!string}
 */
CodeMirror.prototype.getValue = function () {
    "use strict";
};
/**
 * @param {!string} value
 */
CodeMirror.prototype.setValue = function (value) {
    "use strict";
};
/**
 * @type {!number}
 */
CodeMirror.Pass = 3;
/**
 * @param {!CodeMirror.Mode} mode
 * @param {!CodeMirror.State} state
 * @return {!{mode:!CodeMirror.Mode,state:!CodeMirror.State}}
 */
CodeMirror.innerMode = function (mode, state) {
    "use strict";
};
/**
 * @param {?Element} element
 * @param {!Object} options
 * @return {!CodeMirror}
 */
CodeMirror.fromTextArea = function(element, options) {
    "use strict";
};
